CC=gcc
CXX=g++
#COBJS=hidapi/linux/hid-libusb.o
COBJS+=dictionary.o iniparser.o main.o
CPPOBJS=
OBJS=$(COBJS) $(CPPOBJS)
#CFLAGS+=-Ihidapi/hidapi -lpthread -Wall -g -c `pkg-config libusb-1.0 --cflags`
CFLAGS+=-Wall -g
LIBS=#`pkg-config libusb-1.0 libudev --libs`


all: adblockc

adblockc: $(OBJS)
	$(CXX) $(CFLAGS) $^ $(LIBS) -o adblockc

$(COBJS): %.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

$(CPPOBJS): %.o: %.cpp
	$(CXX) $(CFLAGS) -c $< -o $@

install:
	cp adblockc /usr/bin/
	chmod 755 /usr/bin/adblockc
	cp provider.urls /etc/
	chmod 755 /etc/provider.urls

uninstall:
	rm /usr/bin/adblockc
	rm /etc/provider.urls

clean:
	rm -f $(OBJS) adblockc 
