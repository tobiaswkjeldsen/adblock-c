/**
 
Simple and effective adblocker in C. It will block ads (effectively hosts) using the /etc/hosts file. 
Using already excisting tools such as cat and wget etc.. 
Will fetch hosts from known providers or use a specified file with providers.

Copyright (C) 2011 Tobias Westergaard Kjeldsen <tobias@wkjeldsen.dk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/> 

**/

#include <stdio.h>
#include <string.h>

#include "iniparser.h" // A parser able to parse the INI format

char const *PROVIDER_HOSTS_F = "adblock.adhosts";
char const *HOSTS_BACKUP_F = "/etc/adblock.hosts.backup";
char const *HOSTS_OLD_F = "adblock.hosts.old";

char commandExec[1024];

void merge_new_hosts() {
	// We copy original hosts file to a new location
	sprintf(commandExec, "cp /etc/hosts %s", HOSTS_OLD_F);
	system(commandExec);

	bzero(commandExec, 1024);
	
	// We copy (and backup) original hosts file for later revert
	FILE* f;
	if((f = fopen(HOSTS_BACKUP_F, "r")) == NULL ) {
		sprintf(commandExec, "cp /etc/hosts %s", HOSTS_BACKUP_F);
		system(commandExec);
	
		bzero(commandExec, 1024);
	} else {
		fclose(f);
	}
	
	// We merge the original hosts file with a new one containing latest hosts to block
	sprintf(commandExec, "cat %s %s | sort | uniq > /etc/hosts", HOSTS_OLD_F, PROVIDER_HOSTS_F);
	int res = system(commandExec);	

	bzero(commandExec, 1024);

	// We cleanup the temporary files we've made
	sprintf(commandExec, "rm -f %s %s", HOSTS_OLD_F, PROVIDER_HOSTS_F);
	system(commandExec);
	
	if(res==0) {
		printf("SUCCESS!\n");	
		printf("Your hosts file has been merged with the latest provided ad-block hosts.\n");
		printf("You may need to restart your system before blocking of ads will take affect!\n");
	} else {
		printf("FAIL!\n");
		printf("Remember to run with root permissions!\n");
	}
}

int main(int argc, char *argv[]) {
	if (argc > 1) {
                if(strcmp(argv[1], "--help")==0) {
                        printf("adblockc by Tobias Westergaard Kjeldsen <tobias@wkjeldsen.dk>\n\n");
			printf("\tYou can use the following options;\n");
                        printf("\t(Remember; root permissions are required!)\n");
			printf("\t\t--help      See this help information.\n");
                        printf("\t\t--providers-file   Specify a file containing provider URL's.\n");
			printf("\t\t--providers-file-format	See an example of the format the provider file needs to by written in.\n\n");
                        exit(0);
                } else if(argc > 1 && strcmp(argv[1], "--providers-file-format")==0) {
                	printf("Example of the provider file format to follow;\n");
			printf("(Each URL should contain raw hosts in the regular /etc/hosts format.)\n\n");
			printf("\t\t[providers]\n");
			printf("\t\t1=http://hosts-file.net/ad_servers.asp\n");
			printf("\t\t2=..\n");
			printf("\t\t3=..\n\n");
                } else if(argc > 1 && strcmp(argv[1], "--revert")==0) {
                        sprintf(commandExec, "mv %s /etc/hosts", HOSTS_BACKUP_F);
			system(commandExec);
			bzero(commandExec, 1024);
			
			printf("Reverted to original /etc/hosts file.\n");
                } else if(argc > 2 && strcmp(argv[1], "--providers-file")==0) {
			FILE *fp;
			if( ( fp = fopen(argv[2], "r")) == NULL) {
				printf("Could open the providers file; %s\n", argv[2]);	
				fclose(fp);
				exit(1);
			}
			dictionary *ini = iniparser_load(argv[2]); // Parse the provider file in INI format
			
			char providerUrl[1024];
			char sKey[20];
			int i = 1;
			
			while(1) {
				sprintf(sKey, "providers:%d", i);
				sprintf(providerUrl, "%s", iniparser_getstring(ini, sKey, ""));
				if(strlen(providerUrl) < 1) break;
			
				printf("Downloading ad-block hosts from; %s\n", providerUrl);
	
				sprintf(commandExec, "wget --quiet \"%s\" -O - >> %s", providerUrl, PROVIDER_HOSTS_F);
				system(commandExec);
				bzero(commandExec, 1024);	
				
				i++;
			}
			
			merge_new_hosts();
			
			iniparser_freedict(ini);
                } else {
                        printf("Argument(s) given not valid!\n");
                        return 0;
                }
        } else {
		printf("Using default provider; http://hosts-file.net/ad_servers.asp\n");
		
		sprintf(commandExec, "wget --quiet http://hosts-file.net/ad_servers.asp -O - >> %s", PROVIDER_HOSTS_F);
		system(commandExec);

		bzero(commandExec, 1024);

		merge_new_hosts();		
	}
	return 0;	
}
